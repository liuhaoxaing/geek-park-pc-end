import { Component } from 'react';
import { Button, Card, Checkbox, Form, Input, message } from 'antd';
import imgSrc from 'assets/logo.png';
import { loginAPI } from 'api/user';
import styles from './index.module.scss';
import { settoken } from 'utils/storage';

export default class Login extends Component {
  onFinish = async (values) => {
    const { mobile, code } = values;
    try {
      const res = await loginAPI(mobile, code);
      console.log(res, 'res');
      // 本地保存
      settoken(res.data.token);
      // 跳转yemia
      this.props.history.push('/');
      // 提示成功
      // 3. 提示用户 💥 注意不要使用alert
      message.success('登录成功', 1);
    } catch (error) {
      if (error.response) {
        message.warning(error.response.data.message);
      }
    }
  };
  render() {
    return (
      <div className={styles.login}>
        <Card className="login-container">
          <img src={imgSrc} alt="" className="login-logo" />
          <Form
            autoComplete="off"
            onFinish={this.onFinish}
            initialValues={{ mobile: '13811111111', code: '246810' }}
          >
            <Form.Item
              name="mobile"
              rules={[
                {
                  required: true,
                  message: '手机号不能为空',
                },
                {
                  pattern: /^1[3456789]\d{9}$/,
                  message: '请输入正确的手机号码',
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="code"
              rules={[
                {
                  required: true,
                  message: '验证码不能为空',
                },
                {
                  pattern: /^\d{6}$/,
                  message: '请输入正确的验证码',
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              valuePropName="checked"
              name="agree"
              rules={[
                {
                  validator: (_, value) => {
                    if (value === true) {
                      return Promise.resolve();
                    } else {
                      return Promise.reject(new Error('阅读并打勾'));
                    }
                  },
                },
              ]}
            >
              <Checkbox>我已阅读并同意[隐私条款]和[用户协议]</Checkbox>
            </Form.Item>

            <Form.Item>
              <Button block type="primary" htmlType="submit">
                登录
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </div>
    );
  }
}
