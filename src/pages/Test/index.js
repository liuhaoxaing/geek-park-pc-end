import { message, Modal, Upload } from 'antd';
import { useState } from 'react';
import { baseURL } from 'utils/request';
import { gettoken } from 'utils/storage';
import { UploadOutlined } from '@ant-design/icons';

export default function Test() {
  // 1. 声明状态控制Upload的fileList ，等价于表单的value
  const [fileList, setFileList] = useState([]);
  const [imgSrc, setImgSrc] = useState('');
  // 1. 提取了配置props
  const props = {
    // 1.1 name代表后台支持上传文件的类型，值由后台决定
    name: 'image',
    // 文件上传框，默认支持的文件类型
    accept: '.png,.jpg,.jpeg',
    // 1.2 代表上传文件的请求url地址
    action: `${baseURL}/v1_0/upload`,
    // 1.3 加上token
    headers: {
      authorization: `Bearer ${gettoken()}`,
    },
    // 2. onChange事件改变fileList
    onChange(info) {
      console.log('info  ----->  ', info);
      setFileList(info.fileList);
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onPreview(file) {
      setImgSrc(file.thumbUrl);
    },
    // 配置beforeUpload钩子函数
    beforeUpload(file) {
      const imgs = ['image/jpg', 'image/png', 'image/jpeg'];
      if (!imgs.includes(file.type)) {
        message.warning('不允许上传的文件类型', 1);
        return Upload.LIST_IGNORE;
      }
    },
  };
  return (
    <div>
      <Upload {...props} listType="picture-card" fileList={fileList}>
        {/* 4.判断fileList的数量， 是否显示按钮 */}
        {fileList.length === 3 ? null : <UploadOutlined />}
      </Upload>
      <Modal
        maskClosable={false}
        title="预览图片"
        visible={!!imgSrc}
        footer={null}
        onCancel={() => setImgSrc('')}
      >
        <img width={480} style={{ objectFit: 'cover' }} src={imgSrc} alt="" />
      </Modal>
    </div>
  );
}
