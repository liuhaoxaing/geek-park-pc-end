import { Component } from 'react';
import { Layout, Menu, Button, Popconfirm } from 'antd';
import {
  LogoutOutlined,
  HomeOutlined,
  DiffOutlined,
  EditOutlined,
} from '@ant-design/icons';
import styles from './index.module.scss';
import { Switch, Route, Redirect, Link } from 'react-router-dom';
import { deltoken } from 'utils/storage';
import Home from '../Home/index';
import ArticleList from '../ArticleList/index';
import ArticlePublish from '../ArticlePublish/index';
import { getUserInfoAPI } from 'api/user';
const { Header, Sider } = Layout;

export default class MyLayout extends Component {
  state = {
    userInfo: {},
  };
  handleLoginOut = () => {
    // 删除缓存
    deltoken();
    // 跳转页面
    this.props.history.push('/login');
  };
  async componentDidMount() {
    const res = await getUserInfoAPI();
    this.setState({ userInfo: res.data });
    // 获取地址栏参数
    // console.log(this.props.location.pathname);
  }
  render() {
    const pathname = this.props.location.pathname.includes('/edit')
      ? 'list'
      : this.props.location.pathname;
    return (
      <Layout className={styles.layout}>
        <Header className="header">
          <div className="logo" />
          <div className="profile">
            <span>{this.state.userInfo.name}</span>
            <Popconfirm
              placement="bottom"
              title="是否确定退出"
              onConfirm={this.handleLoginOut}
              okText="确定"
              cancelText="取消"
            >
              <Button
                style={{ color: '#fff' }}
                type="text"
                icon={<LogoutOutlined />}
              >
                退出
              </Button>
            </Popconfirm>
          </div>
        </Header>
        <Layout style={{ height: 'calc( 100vh - 64px)' }}>
          <Sider width={200} className="site-layout-background">
            <Menu
              theme="dark"
              mode="inline"
              defaultSelectedKeys={['/home']}
              selectedKeys={[pathname]}
              style={{ height: '100%', borderRight: 0 }}
            >
              <Menu.Item key="/home" icon={<HomeOutlined />}>
                <Link to="/home">数据概览</Link>
              </Menu.Item>
              <Menu.Item key="/list" icon={<DiffOutlined />}>
                <Link to="/list">内容管理</Link>
              </Menu.Item>
              <Menu.Item key="/publish" icon={<EditOutlined />}>
                <Link to="/publish">发布文章</Link>
              </Menu.Item>
            </Menu>
          </Sider>
          {/* <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item>List</Breadcrumb.Item>
              <Breadcrumb.Item>App</Breadcrumb.Item>
            </Breadcrumb> */}
          <Layout>
            <Switch>
              <Redirect from="/" to="/home" exact></Redirect>
              <Route path="/home" component={Home}></Route>
              <Route path="/list" component={ArticleList}></Route>
              <Route
                key="/publish"
                path="/publish"
                component={ArticlePublish}
              ></Route>
              <Route
                key="/edit/:id"
                path="/edit/:id"
                component={ArticlePublish}
              ></Route>
            </Switch>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}
