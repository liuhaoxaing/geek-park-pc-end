import ReactDOM from 'react-dom';
import App from './App';
// 在 src/index.js 中全局导入 antd 的样式文件
import 'antd/dist/antd.css';
import 'moment/locale/zh-cn';
import locale from 'antd/lib/locale/zh_CN';
import { ConfigProvider } from 'antd';

ReactDOM.render(
  <ConfigProvider locale={locale}>
    <App />
  </ConfigProvider>,
  document.getElementById('root')
);
