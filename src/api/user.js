import request from '../utils/request';

/**
 * 登录请求，用于用户登录
 * @param {string} mobile 手机号
 * @param {string} code 验证码
 * @returns Promise
 */
export const loginAPI = (mobile, code) => {
  return request({
    method: 'POST',
    url: '/v1_0/authorizations',
    data: {
      mobile,
      code,
    },
  });
};

export const getUserInfoAPI = () => {
  return request({
    url: '/v1_0/user/profile',
  });
};
