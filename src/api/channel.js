import request from '../utils/request';

export const loadChannelAPI = () => {
  return request({
    url: '/v1_0/channels',
  });
};
