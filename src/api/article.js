// 1. 封装查询文章列表API

import request from 'utils/request';

export const getArticleListAPI = (params) => {
  return request({
    url: '/v1_0/mp/articles',
    params,
  });
};
export const delArticlebyidAPI = (id) => {
  return request({
    url: `/v1_0/mp/articles/${id}`,
    method: 'delete',
  });
};
export const addArticlelistAPI = (data, draft) => {
  return request({
    url: `/v1_0/mp/articles?draft=${draft}`,
    method: 'POST',
    data,
  });
};
export const getArticlebyidAPI = (id) => {
  return request({
    url: `/v1_0/mp/articles/${id}`,
    method: 'GET',
  });
};
export const setArticlebyidAPI = (id, data, draft) => {
  return request({
    url: `/v1_0/mp/articles/${id}?draft=${draft}`,
    method: 'PUT',
    data,
  });
};
