import React, { Component } from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import { AuthRoute } from './components/AuthRoute';
import Layout from './pages/Layout/index';
import Login from './pages/Login/index';
import Test from './pages/Test/index';
import history from 'utils/history';
export default class App extends Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          {/* <Redirect from="/" to="/layout" exact></Redirect> */}
          <Route path="/login" component={Login}></Route>
          <Route path="/test" component={Test}></Route>
          <AuthRoute path="/" component={Layout}></AuthRoute>
        </Switch>
      </Router>
    );
  }
}
