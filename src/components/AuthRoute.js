import React from 'react';
import { Route } from 'react-router-dom';
import { gettoken } from 'utils/storage';

export function AuthRoute(props) {
  const { path, component: Cop } = props;
  return (
    <Route
      path={path}
      render={(props) => {
        // console.log(props);
        if (gettoken()) {
          return <Cop {...props} />;
        }
        props.history.push('/login');
      }}
    ></Route>
  );
}
