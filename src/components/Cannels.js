import { Select } from 'antd';
import { loadChannelAPI } from 'api/channel';
import { useEffect, useState } from 'react';
const { Option } = Select;

export default function Cannles(props) {
  // 2.声明状态存储频道列表
  const [channels, setChannels] = useState([]);
  const loadchannels = async () => {
    const res = await loadChannelAPI();
    setChannels(res.data.channels);
  };
  // 挂载后发请求
  useEffect(() => {
    loadchannels();
  }, []);
  return (
    <Select style={{ width: 200 }} placeholder="请选择频道列表" {...props}>
      {channels.map((item) => {
        return (
          <Option key={item.id} value={item.id}>
            {item.name}
          </Option>
        );
      })}
    </Select>
  );
}
