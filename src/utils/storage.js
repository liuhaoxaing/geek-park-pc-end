const TOKEN_KEY = 'geek-parktoken';

const settoken = (token) => {
  localStorage.setItem(TOKEN_KEY, token);
};

const gettoken = () => localStorage.getItem(TOKEN_KEY);
const deltoken = () => {
  localStorage.removeItem(TOKEN_KEY);
};

export { settoken, gettoken, deltoken };
