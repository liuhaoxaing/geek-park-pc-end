// 1. 从history包中导入createBrowserHistory函数
import { createBrowserHistory } from 'history';
const history = createBrowserHistory();
// 2. 创建history并导出
export default history;
